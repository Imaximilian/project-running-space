﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelGenerator : MonoBehaviour
{

    [Header("Plattform prefabs")]
    public List<GameObject> grassPlattforms;
    public GameObject[] icePlattforms;
    public GameObject[] desertPlattforms;
    public GameObject startplattform;
    public GameObject endplattform;


    [Header("Plattform Spawn Settings")]
    public float plattormAmount = 0;
    public float startTileamount = 0;
    public float offset;
    public float waitTime;
    public List<GameObject> createdPlattforms; //darüber kann ich iterieren und die enemys spawnen lassen

    public float waterchance; //damit nicht zu viele waterplattformen auftauchen
    public float saveChance; //damit nicht zu viele waterplattformen auftauchen
    public float lifechance; //damit nicht zu viele waterplattformen auftauchen

    //check if plattform is already there variables
    private bool minusX = false;
    private bool minusZ = false;
    private bool plusX = false;
    private bool plusZ = false; // false = no collision mit anderer plattform

    // Use this for initialization
    void Start()
    {
        GetCoroutine();
    }

    public void GetCoroutine()
    {
        StartCoroutine(AddPlattforms());
    }

    IEnumerator AddPlattforms()
    {
        GameObject plattform = Instantiate(startplattform, new Vector3(transform.position.x, transform.position.y - 10, transform.position.z), transform.rotation) as GameObject;
        createdPlattforms.Add(plattform);
        while (plattormAmount > 0)
        {
            var rnd = Random.Range(0, 3); //das gibt an welcher seite eine plattform angebaut wird
                                          // var tile = Random.Range(0, grassPlattforms.Count); //eine rnd tile aus dem array
            var rndChance = Random.Range(0f, 1f);
            GameObject tile = getTile(rndChance);
            MoveDirection(rnd, tile, rndChance);

            yield return new WaitForSeconds(waitTime);
        }

        yield return 0;
        GameObject plattform1 = Instantiate(endplattform, new Vector3(transform.position.x, transform.position.y - 10, transform.position.z), transform.rotation) as GameObject;
        createdPlattforms.Add(plattform1);

    }

    private GameObject getTile(float rndchance)
    {
        if (rndchance <= waterchance)//getting water grasstile
        {
            foreach (var item in grassPlattforms)
            {
                if (item.gameObject.name.Contains("Water"))
                {
                    return item;
                }
            }
        }
        if (rndchance > waterchance && rndchance <= saveChance)//getting grassTile
        {
            foreach (var item in grassPlattforms)
            {
                if (item.gameObject.name.Contains("GrassTile"))
                {
                    return item;
                }
            }
        }
        if (rndchance > saveChance && rndchance <= lifechance)//getting checkpoint life tile
        {
            foreach (var item in grassPlattforms)
            {
                if (item.gameObject.name.Contains("CheckPointLife"))
                {
                    return item;
                }
            }
        }
        if (rndchance > lifechance)//getting checkpoint save tile
        {
            foreach (var item in grassPlattforms)
            {
                if (item.gameObject.name.Contains("CheckPointSave"))
                {
                    return item;
                }
            }
        }

        return grassPlattforms[1];


    }

    //abfangen ob da schon eine plattform ist 
    private void MoveDirection(int direction = 0, GameObject rndTile = null, float rndChance = 0)
    {
        if (direction == 0 && minusX == false)//-x
        {
            plusX = true;
            minusX = false;
            transform.position = new Vector3(transform.position.x - offset, transform.position.y, transform.position.z);
            CreatePlattform(rndTile);
        }
        if (direction == 1 && plusZ == false)//+z
        {
            minusZ = true;
            plusZ = false;
            transform.position = new Vector3(transform.position.x, transform.position.y, transform.position.z + offset);
            CreatePlattform(rndTile);
        }
        if (direction == 2 && plusX == false)//+x
        {
            minusX = true;
            plusX = false;
            transform.position = new Vector3(transform.position.x + offset, transform.position.y, transform.position.z);
            CreatePlattform(rndTile);
        }
        if (direction == 3 && minusZ == false)//-z
        {
            plusZ = true;
            minusZ = false;
            transform.position = new Vector3(transform.position.x, transform.position.y, transform.position.z + offset);
            CreatePlattform(rndTile);
        }
        else
        {
            //Debug.Log("redo");
        }
    }

    private void CreatePlattform(GameObject rndTile)
    {
        plattormAmount--;
        GameObject plattform = Instantiate(rndTile, new Vector3(transform.position.x, transform.position.y-10, transform.position.z), transform.rotation) as GameObject;
        createdPlattforms.Add(plattform);
    }


}
